<!DOCTYPE html>
<html>
<title>Atomic Project Home Page</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="Resources/css/w3.css">

<body >

<nav class="w3-sidenav w3-cyan w3-card-2" style="display:none" id="mySidenav">
    <a href="javascript:void(0)"
       onclick="w3_close()"
       class="w3-closenav  w3-large w3-right"> Close &times;</a>
    <div class="w3-padding w3-center w3-teal">
        <img class="w3-circle" src="Resources/Image1/emu1.jpg" alt="emran" style="width:100%">
    </div>
    <br>
    <a href="index.php">Home</a>
    <a href="views/SEIP137033/Book/index.php" target="_blank">Book</a>
    <a href="views/SEIP137033/Hobby/index.php" target="_blank">Hobby</a>
    <a href="views/SEIP137033/Newsletter/index.php" target="_blank">Email</a>
    <a href="views/SEIP137033/ProfilePicture/index.php" target="_blank">ProfilePicture</a>
    <a href="views/SEIP137033/Birthday/index.php" target="_blank">Birthday</a>
    <a href="views/SEIP137033/Gender/index.php" target="_blank">Gender</a>
    <a href="views/SEIP137033/City/index.php" target="_blank">City</a>
    <a href="views/SEIP137033/Summary/index.php" target="_blank">Summary</a>

</nav>

<nav class="w3-sidenav w3-cyan w3-card-2 w3-animate-right" style="display:none;right:0;" id="rightMenu">
    <a href="javascript:void(0)" onclick="closeRightMenu()"
       class="w3-closenav w3-xlarge w3-blue-grey"> &times;</a>
    <a href="about.php">About Project</a>
    <a href="contact.php">Contact</a>
    <a href="index.php">Back</a>
</nav>


<div id="main">

    <header class="w3-container w3-teal">
        <h1>
            <span class="w3-opennav w3-xxlarge" onclick="w3_open()" id="openNav">&#9776;</span>
            Atomic Project Home Page

            <span class="w3-btn-floating-large w3-amber w3-right" onclick="openRightMenu()"><i>+</i></span>
        </h1>
    </header>

    <div class="w3-half w3-light-blue w3-container" >
        <div class="w3-padding-64 w3-center">
            <h1>Submitted By:</h1>
            <img src="Resources/Image1/emu1.jpg" class=" w3-circle" alt="Emran" style="width:50%">
            <div class="w3-left-align w3-padding-large">
                <pre>
                    <h2>
                    Name: Emran Hosen
                    SEIP ID:137033
                    Class Serial: 22
                    </h2>
                </pre>

            </div>
        </div>
    </div>

    <div class="w3-half w3-cyan w3-container" >
        <div class="w3-padding-64 w3-center">
            <h1>Submitted To:</h1>
            <img src="Resources/Image1/yameen.jpg" class=" w3-circle" alt="Yameen" style="width:50%">
            <div class="w3-left-align w3-padding-large">
                <pre>
                    <h2>
                       Name: Yameen Hossain
                       Course Trainer
                       BASIS BITM , Chittagong.
                    </h2>

                </pre>
            </div>
        </div>
    </div>



    <footer class="w3-container w3-teal w3-theme w3-bottom">
        <h3 class="w3-center"> BASIS BITM Atomic Project</h3>
    </footer>

</div>

<script>
    function w3_open() {
        document.getElementById("main").style.marginLeft = "15%";
        document.getElementById("mySidenav").style.width = "15%";
        document.getElementById("mySidenav").style.display = "block";
        document.getElementById("openNav").style.display = 'none';

    }
    function w3_close() {
        document.getElementById("main").style.marginLeft = "0%";
        document.getElementById("mySidenav").style.display = "none";
        document.getElementById("openNav").style.display = "inline-block";

    }

    function openRightMenu() {
        document.getElementById("rightMenu").style.display = "block";
    }
    function closeRightMenu() {
        document.getElementById("rightMenu").style.display = "none";
    }



</script>

</body>

