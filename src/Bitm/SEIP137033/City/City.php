<?php
namespace Bitm\SEIP137033\City;
namespace App\Bitm\SEIP137033\City;
use App\Bitm\SEIP137033\Utility\Utility;
use App\Bitm\SEIP137033\Message\Message;

class City{
    public $id="";
    public $name="";
    public $city="";
    public $conn;
    public $deleted_at;

    public function prepare($data=""){
        if(array_key_exists("id",$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists("name",$data)){
            $this->name=$data['name'];
        }

        if(array_key_exists("city",$data)){
            $this->city=$data['city'];
        }
    }

    public function __construct()
    {
        $this->conn=mysqli_connect("localhost","root","","atomicprojectb22") or die("Database Connection Failed");
    }

    public function store(){
        $query="INSERT INTO `atomicprojectb22`.`city` (`name`,`city`) VALUES ('".$this->name."','".$this->city."')";
//        echo $query;
//        die();
        $result=mysqli_query($this->conn,$query);
        if($result){
            Message::message("
            <div class=\"w3-container w3-section w3-khaki w3-round\">
               <h4><strong>Success!</strong> Data has been stored  successfully.</h4>
            </div>");
            // echo "Data Sent Succesfully";
            Utility::redirect("index.php");
        } else {
            Message::message("
                <div class=\"w3-container w3-section w3-khaki w3-round\">
                  <h4><strong>Sorry!</strong> Data has not been stored successfully.</h4>
                </div>");
            Utility::redirect("index.php");
        }

    }

    public function index(){
        $_allCity= array();
        $query="SELECT * FROM `city` WHERE `deleted_at` IS NULL";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_object($result)){
            $_allCity[]=$row;
        }
        return $_allCity;
    }

    public function view(){
        $query="SELECT * FROM `city` WHERE `id`=".$this->id;
        $result=mysqli_query($this->conn,$query);
        $row=mysqli_fetch_object($result);
        return $row;

    }

    public function update(){
        $query="UPDATE `city` SET `name` = '". $this->name ."', `city` = '".$this->city."' WHERE `city`.`id` = ".$this->id;
        $result=mysqli_query($this->conn,$query);
        if($result){
            Message::message("
            <div class=\"w3-container w3-section w3-khaki w3-round\">
              <h4><strong>Success!</strong> Data has been updated  successfully.</h4>
            </div>");
            Utility::redirect("index.php");
        }
        else {
            Message::message("
                <div class=\"w3-container w3-section w3-khaki w3-round\">
                  <h4><strong>Sorry!</strong> Data has not been updated successfully.</h4>
                </div>");
            Utility::redirect("index.php");
        }
    }

    public function delete(){
        $query="DELETE FROM `atomicprojectb22`.`city` WHERE `city`.`id` =".$this->id;
        $result=mysqli_query($this->conn,$query);
        if ($result) {
            Message::message("
               <div class=\"w3-container w3-section w3-khaki w3-round\">
                <h4><strong>Deleted!</strong> Data has been deleted successfully.</h4>
               </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
                <div class=\"w3-container w3-section w3-khaki w3-round\">
                  <h4><strong>Sorry!</strong> Data has not been deleted successfully.</h4>
                </div>");
            Utility::redirect("index.php");
        }
    }

    public function trash()
    {
        $this->deleted_at = time();
        $query = "UPDATE `atomicprojectb22`.`city` SET `deleted_at` =" . $this->deleted_at . " WHERE `city`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class=\"w3-container w3-section w3-khaki w3-round\">
                  <h4><strong>Deleted!</strong> Data has been Trashed successfully.</h4>
                </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
                <div class=\"w3-container w3-section w3-khaki w3-round\">
                  <h4><strong>Sorry!</strong> Data has not been trashed successfully.</h4>
                </div>");
            Utility::redirect("index.php");
        }

    }

    public function trashed(){
        $_allCity= array();
        $query="SELECT * FROM `city` WHERE `deleted_at` IS NOT NULL";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_allCity[]=$row;
        }
        return $_allCity;
    }

    public function recover(){
        $this->deleted_at=time();
        $query="UPDATE `city` SET `deleted_at` = NULL WHERE `city`.`id` =".$this->id;
//        echo $query;
//        die();
        $result=mysqli_query($this->conn,$query);
        if ($result) {
            Message::message("
                <div class=\"w3-container w3-section w3-khaki w3-round\">
                  <h4><strong>Success!</strong> Data has been recovered successfully.</h4>
                </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
                <div class=\"w3-container w3-section w3-khaki w3-round\">
                  <h4><strong>Sorry!</strong> Data has not been recovered successfully.</h4>
                </div>");
            Utility::redirect("index.php");
        }
    }

    public function recoverSelected($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "UPDATE `atomicprojectb22`.`city` SET `deleted_at` = NULL WHERE `city`.`id` IN(" . $ids . ")";
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
                    <div class=\"w3-container w3-section w3-khaki w3-round\">
                      <h4><strong>Deleted!</strong> Selected Data has been recovered successfully.</h4>
                    </div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
                    <div class=\"w3-container w3-section w3-khaki w3-round\">
                      <h4><strong>Oops!</strong> Selected Data has not been recovered successfully.</h4>
                    </div>");
                Utility::redirect("index.php");
            }

        }

    }

    public function deleteSelected($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "DELETE FROM `atomicprojectb22`.`city` WHERE `city`.`id`  IN(" . $ids . ")";
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
                      <div class=\"w3-container w3-section w3-khaki w3-round\">
                         <h4><strong>Deleted!</strong> Selected Data has been deleted successfully.</h4>
                       </div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
                    <div class=\"w3-container w3-section w3-khaki w3-round\">
                      <h4><strong>Oops!</strong> Selected Data has not been deleted successfully.</h4>
                    </div>");
                Utility::redirect("index.php");
            }

        }
    }


    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `atomicprojectb22`.`city` WHERE deleted_at IS NULL";
        $result=mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }
    public function paginator($pageStartFrom,$Limit){
        $_allCity = array();
        $query="SELECT * FROM `city` WHERE deleted_at IS NULL LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allCity[] = $row;
        }

        return $_allCity;

    }





}



?>