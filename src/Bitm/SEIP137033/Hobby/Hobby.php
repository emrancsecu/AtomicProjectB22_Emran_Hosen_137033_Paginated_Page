<?php
namespace App\Bitm\SEIP137033\Hobby;
use App\Bitm\SEIP137033\Message\Message;
use App\Bitm\SEIP137033\Utility\Utility;

class Hobby{

    public $id="";
    public $name="";
    public $hobby="";
    public $conn;
    public $deleted_at;


    public function prepare($data=""){
        if(array_key_exists("name",$data)){
            $this->name=$data['name'];
        }
        if(array_key_exists("id",$data)){
            $this->id=$data['id'];
        }
    }


    public function __construct()
    {
        $this->conn=mysqli_connect("localhost","root","","atomicprojectb22") or die("Database Connection Failed");
    }

    public function store($hobbies = Array())
    {
        if ((is_array($hobbies)) && (count($hobbies > 0))) {
            $userHobby = implode(",", $hobbies);
//            var_dump($hobby);
//            die();
            $query = "INSERT INTO `atomicprojectb22`.`hobby` (`name`,`hobbies`) VALUES ('".$this->name."', '".$userHobby."')";
//            echo $query;
//            die();

            $result = mysqli_query($this->conn, $query);

            if ($result) {
                Message::message("
            <div class=\"w3-container w3-section w3-khaki w3-round\">
               <h4><strong>Success!</strong> Data has been stored  successfully.</h4>
            </div>");
                // echo "Data Sent Succesfully";
                Utility::redirect("index.php");
            } else {
                Message::message("
                <div class=\"w3-container w3-section w3-khaki w3-round\">
                  <h4><strong>Sorry!</strong> Data has not been stored successfully.</h4>
                </div>");
                Utility::redirect("index.php");
            }
        }

    }

    public function index(){
        $hobbies= array();
        $query="SELECT * FROM `hobby` WHERE deleted_at IS NULL";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_object($result)){
            $hobbies[]=$row;
        }
        return $hobbies;
    }

    public function view(){
        $query="SELECT * FROM `hobby` WHERE `id`=".$this->id;

        //Utility::dd($query);
        //die();

        $result=mysqli_query($this->conn,$query);
        $row=mysqli_fetch_object($result);
        //echo $row;
        return $row;

    }

    public function update($hobbies = Array())
    {
        if ((is_array($hobbies)) && (count($hobbies > 0))) {
            $allHobby = implode(",", $hobbies);
//          var_dump($hobby);
//          die();
            $query = "UPDATE `atomicprojectb22`.`hobby` SET `name` = '$this->name', `hobbies` = '". $allHobby ."' WHERE `hobby`.`id` =". $this->id;
//            echo $query;
//            die();
            $result = mysqli_query($this->conn, $query);

            if ($result) {
                Message::message("
            <div class=\"w3-container w3-section w3-khaki w3-round\">
              <h4><strong>Success!</strong> Data has been updated  successfully.</h4>
            </div>");
                Utility::redirect("index.php");
            }
            else {
                Message::message("
                <div class=\"w3-container w3-section w3-khaki w3-round\">
                  <h4><strong>Sorry!</strong> Data has not been updated successfully.</h4>
                </div>");
                Utility::redirect("index.php");
            }
        }

    }

    public function delete(){
        $query="DELETE FROM `atomicprojectb22`.`hobby` WHERE `hobby`.`id` =".$this->id;
        $result=mysqli_query($this->conn,$query);
        if ($result) {
            Message::message("
               <div class=\"w3-container w3-section w3-khaki w3-round\">
                <h4><strong>Deleted!</strong> Data has been deleted successfully.</h4>
               </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
                <div class=\"w3-container w3-section w3-khaki w3-round\">
                  <h4><strong>Sorry!</strong> Data has not been deleted successfully.</h4>
                </div>");
            Utility::redirect("index.php");
        }
    }


    public function trash()
    {
        $this->deleted_at = time();
        $query = "UPDATE `atomicprojectb22`.`hobby` SET `deleted_at` =" . $this->deleted_at . " WHERE `hobby`.`id` = " . $this->id;
        //Utility::dd($query);

        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class=\"w3-container w3-section w3-khaki w3-round\">
                  <h4><strong>Deleted!</strong> Data has been Trashed successfully.</h4>
                </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
                <div class=\"w3-container w3-section w3-khaki w3-round\">
                  <h4><strong>Sorry!</strong> Data has not been trashed successfully.</h4>
                </div>");
            Utility::redirect("index.php");
        }

    }

    public function trashed(){
        $_allHobby= array();
        $query="SELECT * FROM `hobby` WHERE `deleted_at` IS NOT NULL";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_allHobby[]=$row;
        }
        return $_allHobby;
    }


    public function recover(){
        $this->deleted_at=time();
        $query="UPDATE `hobby` SET `deleted_at` = NULL WHERE `hobby`.`id` =".$this->id;
//        echo $query;
//        die();
        $result=mysqli_query($this->conn,$query);
        if ($result) {
            Message::message("
                <div class=\"w3-container w3-section w3-khaki w3-round\">
                  <h4><strong>Success!</strong> Data has been recovered successfully.</h4>
                </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
                <div class=\"w3-container w3-section w3-khaki w3-round\">
                  <h4><strong>Sorry!</strong> Data has not been recovered successfully.</h4>
                </div>");
            Utility::redirect("index.php");
        }
    }


    public function recoverSelected($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "UPDATE `atomicprojectb22`.`hobby` SET `deleted_at` = NULL WHERE `hobby`.`id` IN(" . $ids . ")";
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
                    <div class=\"w3-container w3-section w3-khaki w3-round\">
                      <h4><strong>Deleted!</strong> Selected Data has been recovered successfully.</h4>
                    </div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
                    <div class=\"w3-container w3-section w3-khaki w3-round\">
                      <h4><strong>Oops!</strong> Selected Data has not been recovered successfully.</h4>
                    </div>");
                Utility::redirect("index.php");
            }

        }

    }



    public function deleteSelected($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "DELETE FROM `atomicprojectb22`.`hobby` WHERE `hobby`.`id`  IN(" . $ids . ")";
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
                      <div class=\"w3-container w3-section w3-khaki w3-round\">
                         <h4><strong>Deleted!</strong> Selected Data has been deleted successfully.</h4>
                       </div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
                    <div class=\"w3-container w3-section w3-khaki w3-round\">
                      <h4><strong>Oops!</strong> Selected Data has not been deleted successfully.</h4>
                    </div>");
                Utility::redirect("index.php");
            }

        }
    }

    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `atomicprojectb22`.`hobby` WHERE deleted_at IS NULL";
        $result=mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }
    public function paginator($pageStartFrom,$Limit){
        $_allBook = array();
        $query="SELECT * FROM `hobby` WHERE deleted_at IS NULL LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allHobby[] = $row;
        }

        return $_allHobby;

    }




}

?>