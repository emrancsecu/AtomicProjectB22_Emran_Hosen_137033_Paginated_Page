-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 21, 2016 at 01:00 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomicprojectb22`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE `birthday` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `deleted_at` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `name`, `date`, `deleted_at`) VALUES
(15, 'bbnnh', '2016-06-16', NULL),
(17, 'vbcvbb', '2016-06-02', NULL),
(18, 'vcbsw', '2016-06-09', NULL),
(19, 'sssssss', '2016-06-23', NULL),
(20, 'Emran Hosen', '2016-06-04', NULL),
(21, 'samsu', '2016-06-05', NULL),
(22, 'miraj', '2016-06-06', NULL),
(23, 'cvvb', '2016-06-07', NULL),
(24, 'nhfggh', '2016-06-08', NULL),
(25, 'mmmm', '2016-06-09', NULL),
(26, 'mkl', '2016-06-10', NULL),
(27, 'mokul', '2016-06-11', NULL),
(28, 'bokul', '2016-06-12', NULL),
(29, 'kalam', '2016-06-13', NULL),
(30, 'kasem', '2016-06-14', NULL),
(31, 'samims', '2016-06-15', NULL),
(32, 'faruk', '2016-06-16', NULL),
(33, 'parves', '2016-06-17', NULL),
(34, 'akter', '2016-06-17', NULL),
(35, 'dalim', '2016-06-18', NULL),
(36, 'nadim', '2016-06-19', NULL),
(37, 'islam', '2016-06-20', NULL),
(38, 'jakir', '2016-06-21', NULL),
(39, 'akash', '2016-06-22', NULL),
(40, 'himel', '2016-06-23', NULL),
(41, 'konok', '2016-06-24', NULL),
(42, 'sweet', '2016-06-25', NULL),
(43, 'osim', '2016-06-29', NULL),
(44, 'ibrahim', '2016-06-29', NULL),
(45, 'sadin', '2016-06-30', NULL),
(46, 'bipul', '2016-07-01', NULL),
(47, 'tonmoy', '2016-07-22', NULL),
(48, 'jamal', '2016-06-02', NULL),
(49, 'fdggb', '2016-06-02', NULL),
(50, 'bookor', '2016-06-15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE `book` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `deleted_at` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`id`, `title`, `deleted_at`) VALUES
(88, 'à¦®à¦¹à¦¾à¦—à§à¦°à¦¨à§à¦¥ à¦†à¦² à¦•à§‹à¦°à¦†à¦¨', NULL),
(89, 'à¦¬à¦¾à¦‡à¦¬à§‡à¦²: à¦‡à¦‚à¦°à§‡à¦œà§€', NULL),
(90, 'à¦¶à§à¦°à§€à¦®à§Žà¦­à¦—à¦¬à¦¤ à¦—à§€à¦¤à¦¾', NULL),
(91, 'à¦¬à§‹à¦–à¦¾à¦°à§€ à¦¶à¦°à§€à¦«', NULL),
(92, 'A Tale of Two Cities', NULL),
(93, 'The Lord of the rings', NULL),
(94, 'The Hobbit', NULL),
(95, 'The Little Prince', NULL),
(96, 'Harry potter and the Philosfers Stone', NULL),
(97, 'And Then there were none', NULL),
(98, 'She', NULL),
(99, 'à¦•à§ƒà¦¤à¦¦à¦¾à¦¸à§‡à¦° à¦¹à¦¾à¦¸à¦¿', NULL),
(100, 'à¦«à§à¦² à¦¬à¦‰', NULL),
(101, 'à¦¨à¦¿à¦¶à¦¿ à¦•à§à¦Ÿà§à¦®à§à¦¬', NULL),
(102, 'à¦•à§à¦°à¦¾à¦šà§‡à¦° à¦•à¦°à§à¦¨à§‡à¦²', NULL),
(103, 'à¦†à¦®à¦¿ à¦¤à¦ªà§', NULL),
(104, 'à¦†à¦•à¦¾à¦¶ à¦¬à¦¾à§œà¦¿à§Ÿà§‡ à¦¦à¦¾à¦“', NULL),
(105, 'à¦†à¦®à¦¾à¦° à¦¬à¦¨à§à¦§à§ à¦°à¦¾à¦¶à§‡à¦¦', NULL),
(106, 'à¦†à¦®à¦°à¦¾ à¦¹à§‡à¦à¦Ÿà§‡à¦›à¦¿ à¦¯à¦¾à¦°à¦¾', NULL),
(107, 'à¦…à¦²à§€à¦• à¦®à¦¾à¦¨à§à¦·', NULL),
(108, 'à¦‰à¦ªà¦¨à¦¿à¦¬à§‡à¦¶', NULL),
(109, 'à¦œà§€à¦¬à¦¨ à¦†à¦®à¦¾à¦° à¦¬à§‹à¦¨', NULL),
(110, 'à¦¸à¦‚à¦¶à¦ªà§à¦¤à¦•', NULL),
(111, 'à¦¶à¦¾à¦ª à¦®à§‹à¦šà¦¨', NULL),
(112, 'à¦ˆà¦¶à§à¦¬à¦° à¦ªà§ƒà¦¥à¦¿à¦¬à§€ à¦­à¦¾à¦²à§‹à¦¬', NULL),
(113, 'à¦¹à¦¾à¦œà¦¾à¦° à¦šà§à¦°à¦¾à¦¶à¦¿à¦° à¦®à¦¾', NULL),
(114, 'à¦®à¦¾à¦§à§à¦•à¦°à§€', NULL),
(115, 'à¦ªà¦žà§à¦šà¦® à¦ªà§à¦°à§à¦·', NULL),
(116, 'à¦¦à§‡à¦¶ à¦¬à¦¿à¦¦à§‡à¦¶à§‡', NULL),
(117, 'à¦¤à¦¿à¦¥à¦¿à¦¡à§‹à¦°', NULL),
(118, 'à¦¸à¦¾à¦¤à¦•à¦¾à¦¹à¦¨', NULL),
(119, 'à¦—à¦°à§à¦­à¦§à¦¾à¦°à¦¿à¦£à§€', NULL),
(120, 'à¦¶à§‡à¦· à¦¬à¦¿à¦•à§‡à¦²à§‡à¦° à¦®à§‡à§Ÿà§‡', NULL),
(121, 'à¦¨à§‚à¦°à¦œà¦¾à¦¹à¦¾à¦¨', NULL),
(122, 'à¦ªà¦¦à§à¦®à¦¾ à¦¨à¦¦à§€à¦° à¦®à¦¾à¦à¦¿', NULL),
(123, 'à¦ªà§à¦¤à§à¦² à¦¨à¦¾à¦šà§‡à¦° à¦‡à¦¤à¦¿à¦•à¦¥à¦¾', NULL),
(124, 'à¦†à¦—à§à¦¨ à¦ªà¦¾à¦–à¦¿', NULL),
(125, 'à¦¸à§‚à¦°à§à¦¯ à¦¦à§€à¦˜à¦² à¦¬à¦¾à§œà¦¿', NULL),
(126, 'à¦®à§‡à¦®à¦¸à¦¾à¦¹à§‡à¦¬', NULL),
(127, 'à¦¨à¦¾à¦°à§€', NULL),
(128, 'à¦›à¦¾à¦ªà§à¦ªà¦¾à¦¨à§à¦¨à§‹ à¦¹à¦¾à¦œà¦¾à¦° à¦¬', NULL),
(129, 'à¦¨à¦¨à§à¦¦à¦¿à¦¤ à¦¨à¦°à¦•à§‡', NULL),
(130, 'à¦¶à¦™à§à¦–à¦¨à§€à¦² à¦•à¦¾à¦°à¦¾à¦—à¦¾à¦°', NULL),
(131, 'à¦œà§‹à§Žà¦¸à§à¦¨à¦¾ à¦“ à¦œà¦¨à¦¨à§€à¦° à¦—à¦²à§', NULL),
(132, 'à¦ªà§à¦°à¦¥à¦® à¦†à¦²à§‹', NULL),
(133, 'à¦¸à§‡à¦‡ à¦¸à¦®à§Ÿ', NULL),
(134, 'à¦ªà§‚à¦°à§à¦¬-à¦ªà¦¶à§à¦šà¦¿à¦®', NULL),
(135, 'à¦°à¦¾à¦‡à¦«à§‡à¦² à¦°à§‹à¦Ÿà¦¿ à¦†à¦“à¦°à¦¾à¦¤', NULL),
(136, 'à¦–à§‡à¦²à¦¾à¦°à¦¾à¦® à¦–à§‡à¦²à§‡ à¦¯à¦¾', NULL),
(137, 'à¦¨à¦¿à¦·à¦¿à¦¦à§à¦§ à¦²à§‹à¦¬à¦¾à¦¨', NULL),
(138, 'à¦•à§‡à¦°à§€ à¦¸à¦¾à¦¹à§‡à¦¬à§‡à¦° à¦®à§à¦¨à§à¦¸', NULL),
(139, 'à¦ªà¦¾à¦°à§à¦¥à¦¿à¦¬', NULL),
(140, 'à¦¦à§‚à¦°à¦¬à§€à¦¨', NULL),
(141, 'à¦“à¦¦à§‡à¦° à¦œà¦¾à¦¨à¦¿à§Ÿà§‡ à¦¦à¦¾à¦“', NULL),
(142, 'à¦¤à¦¿à¦¤à¦¾à¦¸ à¦à¦•à¦Ÿà¦¿ à¦¨à¦¦à§€à¦° à¦¨à¦¾à¦', NULL),
(143, 'à¦¨ à¦¹à¦¨à§à¦¯à¦¤à§‡', NULL),
(144, 'à¦•à§œà¦¿ à¦¦à¦¿à§Ÿà§‡ à¦•à¦¿à¦¨à¦²à¦¾à¦®', NULL),
(145, 'à¦²à§‹à¦Ÿà¦¾ à¦•à¦®à§à¦¬à¦²', NULL),
(146, 'à¦…à¦¸à¦®à¦¾à¦ªà§à¦¤ à¦†à¦¤à§à¦®à¦œà§€à¦¬à¦¨à§€', NULL),
(147, 'à¦•à¦¾à¦¬à¦¿à¦²à§‡à¦° à¦¬à§‹à¦¨', NULL),
(148, 'à¦‰à¦ªà¦®à¦¹à¦¾à¦¦à§‡à¦¶', NULL),
(149, 'à¦¤à§‡à¦‡à¦¶ à¦¨à¦®à§à¦¬à¦° à¦¤à§ˆà¦²à¦šà¦¿à¦¤à§', NULL),
(150, 'à¦¦à§ƒà¦·à§à¦Ÿà¦¿à¦ªà¦¾à¦¤', NULL),
(151, 'à¦¹à¦¾à¦œà¦¾à¦° à¦¬à¦›à¦° à¦§à¦°à§‡', NULL),
(152, 'à¦²à¦¾à¦²à¦¸à¦¾à¦²à§', NULL),
(153, 'à¦¤à¦¬à§à¦“ à¦à¦•à¦¦à¦¿à¦¨', NULL),
(154, 'à¦•à¦¬à¦¿', NULL),
(155, 'à¦¶à§à¦¨ à¦¬à¦°à¦¨à¦¾à¦°à§€', NULL),
(156, 'à¦–à§‹à§Ÿà¦¾à¦¬à¦¨à¦¾à¦®à¦¾', NULL),
(157, 'à¦…à¦¨à§à¦¤à¦°à§à¦²à§€à¦¨à¦¾', NULL),
(158, 'à¦²à§Œà¦¹à¦•à¦ªà¦¾à¦Ÿ', NULL),
(159, 'à¦ªà§à¦°à¦¦à§‹à¦·à§‡ à¦ªà§à¦°à¦¾à¦•à§ƒà¦¤à¦œà¦¨', NULL),
(160, 'à¦¶à§‡à¦·à§‡à¦° à¦•à¦¬à¦¿à¦¤à¦¾', NULL),
(161, 'à¦—à§‹à§œà¦¾', NULL),
(162, 'à¦†à¦®à¦¿ à¦¬à¦¿à¦œà§Ÿ à¦¦à§‡à¦–à§‡à¦›à¦¿', NULL),
(163, 'à¦à¦•à¦¾à¦¤à§à¦¤à¦°à§‡à¦° à¦¦à¦¿à¦¨à¦—à§à¦²à¦¿', NULL),
(164, 'Bangladesh a Legacy of Blood', NULL),
(165, ' The Rape of Bangladesh ', NULL),
(166, 'The cruel Birth of Bangladesh', NULL),
(167, 'à¦¬à¦¾à¦‚à¦²à¦¾à¦¦à§‡à¦¶à§‡à¦° à¦¸à§à¦¬à¦¾à¦§à§€à', NULL),
(168, 'à¦ªà§à¦°à¦¬à¦¾à¦¸à§‡ à¦®à§à¦•à§à¦¤à¦¿à¦¯à§à¦¦à', NULL),
(169, 'à¦à¦•à¦¾à¦¤à§à¦¤à¦°-à¦¨à¦¿à¦°à§à¦¯à¦¾à¦¤à¦¨à§‡à', NULL),
(170, 'à¦®à§à¦•à§à¦¤à¦¿à¦¯à§à¦¦à§à¦§à§‡à¦° à¦…à¦ªà§à', NULL),
(171, 'à§§à§¯à§­à§§ à¦¸à¦¾à¦²à§‡ à¦‰à¦¤à§à¦¤à¦° à¦°à¦£à¦', NULL),
(172, 'à¦†à¦®à¦¿ à¦¬à¦¿à¦œà§Ÿ à¦¦à§‡à¦–à¦¤à§‡ à¦šà¦¾à¦‡', NULL),
(173, ' à¦¸à§à¦¬à¦¾à¦§à§€à¦¨à¦¤à¦¾ à¦¸à¦‚à¦—à§à¦°à¦¾à¦®', NULL),
(174, 'à¦®à§à¦•à§à¦¤à¦¿à¦¯à§à¦¦à§à¦§à§‡à¦° à¦°à§‚à¦ªà', NULL),
(175, 'à¦¸à§à¦¬à¦¾à¦§à§€à¦¨à¦¤à¦¾ à¦¯à§à¦¦à§à¦§à§‡ à¦¬', NULL),
(176, 'à¦®à¦¾à¦¨à¦¬à¦¤à¦¾ à¦“ à¦—à¦£à¦®à§à¦•à§à¦¤à¦¿', NULL),
(177, 'à¦œà¦¾à¦—à§à¦°à¦¤ à¦¬à¦¾à¦‚à¦²à¦¾à¦¦à§‡à¦¶', NULL),
(178, 'à¦®à§à¦•à§à¦¤à¦¿à¦¯à§à¦¦à§à¦§à§‡à¦° à¦­à¦¿à¦¨à', NULL),
(179, 'à¦œà§€à¦¬à¦¨à§‡à¦° à¦¯à§à¦¦à§à¦§ à¦¯à§à¦¦à§à¦§', NULL),
(180, 'à¦¸à§à¦¬à¦¾à¦§à§€à¦¨à¦¤à¦¾ â€˜à§­à§§ ', NULL),
(181, 'à¦¯à¦–à¦¨ à¦ªà¦²à¦¾à¦¤à¦•: à¦®à§à¦•à§à¦¤à¦¿à¦¯à§', NULL),
(182, 'à¦¸à§à¦¬à¦¾à¦§à§€à¦¨ à¦¬à¦¾à¦‚à¦²à¦¾à¦° à¦…à¦­à§', NULL),
(183, 'à¦¯à§à¦¦à§à¦§à¦¦à¦¿à¦¨à§‡à¦° à¦•à¦¥à¦¾ ', NULL),
(184, 'à¦¸à¦¿à¦²à§‡à¦Ÿà§‡ à¦—à¦£à¦¹à¦¤à§à¦¯à¦¾', NULL),
(185, 'à¦¸à¦¿à¦²à§‡à¦Ÿà§‡à¦° à¦¯à§à¦¦à§à¦§à¦•à¦¥à¦¾', NULL),
(186, 'à¦®à§à¦•à§à¦¤à¦¿à¦¯à§à¦¦à§à¦§ à¦¸à§à¦¬à¦¾à¦§à', NULL),
(187, ' à¦¸à§à¦¬à¦¾à¦§à§€à¦¨à¦¤à¦¾ à¦¸à¦‚à¦—à§à¦°à¦¾à¦®', NULL),
(190, 'à¦à¦•à¦¾à¦¤à§à¦¤à¦° à¦•à¦¥à¦¾ à¦¬à¦²à§‡', NULL),
(191, 'à¦‡à¦°à¦¿à¦¨à¦¾', NULL),
(194, 'à¦¹à¦¿à¦®à§', NULL),
(195, 'à¦•à¦¬à¦°', NULL),
(196, 'à¦˜à¦°à§‡ à¦¬à¦¾à¦‡à¦°à§‡', NULL),
(197, 'à¦¦à§à¦°à§à¦®à¦°', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `deleted_at` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `city`, `deleted_at`) VALUES
(1, 'vbvb', 'Dhaka', NULL),
(2, 'vbbnvn', 'Comilla', NULL),
(3, 'cvbv', 'Dhaka', NULL),
(4, 'ttt', 'Chittagong', NULL),
(5, 'gcs', 'Rajshahi', NULL),
(6, 'lihiuh', 'Comilla', NULL),
(7, 'esdffsf', 'Barisal', NULL),
(8, 'bhfvg', 'Khulna', NULL),
(9, 'nmmjhng', 'Mymensingh', NULL),
(10, 'iukkh', 'Shylhet', NULL),
(11, 'qww', 'Kishorgonj', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `deleted_at` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `email`, `deleted_at`) VALUES
(34, 'rosetteqr685@hotmail.com', NULL),
(35, 'pearlenedq409@hotmail.com', NULL),
(36, 'shelbydv770@hotmail.com', NULL),
(37, 'shanellebj021@hotmail.com', NULL),
(38, 'shantahn850@hotmail.com', NULL),
(39, 'samathauv659@hotmail.com', NULL),
(40, 'seanhj402@hotmail.com', NULL),
(41, 'sibyldk244@hotmail.com', NULL),
(42, 'sachikoxh430@hotmail.com', NULL),
(43, 'simael342@hotmail.com', NULL),
(44, 'sindycj936@hotmail.com', NULL),
(45, 'shontant212@hotmail.com', NULL),
(46, 'shirelyku708@hotmail.com', NULL),
(47, 'soledadzb206@hotmail.com', NULL),
(48, 'sinamo817@hotmail.com', NULL),
(49, 'shaecs253@hotmail.com', NULL),
(50, 'shandiqc786@hotmail.com', NULL),
(51, 'stacyfa259@hotmail.com', NULL),
(52, 'shylabh288@hotmail.com', NULL),
(53, 'nakiafi587@hotmail.com', NULL),
(54, 'soledadkt441@hotmail.com', NULL),
(55, 'sixtadq771@hotmail.com', NULL),
(56, 'shilarz727@hotmail.com', NULL),
(57, 'samanthaiy206@hotmail.com', NULL),
(58, 'sadeny289@hotmail.com', NULL),
(59, 'stasiawx281@hotmail.com', NULL),
(60, 'starrhw357@hotmail.com', NULL),
(61, 'sumikovx311@hotmail.com', NULL),
(62, 'tamahw549@hotmail.com', NULL),
(63, 'stormyjn410@hotmail.com', NULL),
(64, 'taniamc884@hotmail.com', NULL),
(65, 'sylviauj364@hotmail.com', NULL),
(66, 'tainayj145@hotmail.com', NULL),
(67, 'tabithafw875@hotmail.com', NULL),
(68, 'sukej136@hotmail.com', NULL),
(69, 'taiqf226@hotmail.com', NULL),
(70, 'templedw983@hotmail.com', NULL),
(71, 'sybleln171@hotmail.com', NULL),
(72, 'templehm637@hotmail.com', NULL),
(73, 'tawandair930@hotmail.com', NULL),
(74, 'tericazs842@hotmail.com', NULL),
(75, 'tawannabk733@hotmail.com', NULL),
(76, 'sorayajb064@hotmail.com', NULL),
(77, 'syreetahc019@hotmail.com', NULL),
(78, 'siobhanfj745@hotmail.com', NULL),
(79, 'patriaba126@hotmail.com', NULL),
(80, 'tashadd629@hotmail.com', NULL),
(81, 'shanaeyb559@hotmail.com', NULL),
(82, 'sachaln666@hotmail.com', NULL),
(83, 'sunnihs881@hotmail.com', NULL),
(84, 'sonom451@hotmail.com', NULL),
(85, 'theresaiq885@hotmail.com', NULL),
(86, 'tanikawd530@hotmail.com', NULL),
(87, 'taishatz634@hotmail.com', NULL),
(88, 'sallyzy327@hotmail.com', NULL),
(89, 'thomasinaks208@hotmail.com', NULL),
(90, 'thomasinegy523@hotmail.com', NULL),
(91, 'tanishasr914@hotmail.com', NULL),
(92, 'staceyqf905@hotmail.com', NULL),
(93, 'thoraey021@hotmail.com', NULL),
(94, 'thedatp849@hotmail.com', NULL),
(95, 'tamaralj370@hotmail.com', NULL),
(96, 'tierawb354@hotmail.com', NULL),
(97, 'sundayfv926@hotmail.com', NULL),
(98, 'theressaxm930@hotmail.com', NULL),
(99, 'tomasawi650@hotmail.com', NULL),
(100, 'terrydy839@hotmail.com', NULL),
(101, 'tonyqn789@hotmail.com', NULL),
(102, 'toriese523@hotmail.com', NULL),
(103, 'tuyetuv324@hotmail.com', NULL),
(104, 'tovask313@hotmail.com', NULL),
(105, 'tamalanl742@hotmail.com', NULL),
(106, 'twylaph740@hotmail.com', NULL),
(107, 'verniewz980@hotmail.com', NULL),
(108, 'scottua664@hotmail.com', NULL),
(109, 'tommyens163@hotmail.com', NULL),
(110, 'tracixf514@hotmail.com', NULL),
(111, 'veolamy844@hotmail.com', NULL),
(112, 'tonjacb559@hotmail.com', NULL),
(113, 'vickeydd538@hotmail.com', NULL),
(114, 'traceytx889@hotmail.com', NULL),
(115, 'vickiiq990@hotmail.com', NULL),
(116, 'veolajw069@hotmail.com', NULL),
(117, 'tobyfr228@hotmail.com', NULL),
(118, 'virgilsn276@hotmail.com', NULL),
(119, 'violetvi552@hotmail.com', NULL),
(120, 'valeriaoa227@hotmail.com', NULL),
(121, 'vannaay178@hotmail.com', NULL),
(122, 'victorinaos149@hotmail.com', NULL),
(123, 'waicc962@hotmail.com', NULL),
(124, 'wanitazy138@hotmail.com', NULL),
(125, 'willettezb895@hotmail.com', NULL),
(126, 'willettegg800@hotmail.com', NULL),
(127, 'tovajg763@hotmail.com', NULL),
(128, 'tynishadh440@hotmail.com', NULL),
(129, 'willettaew652@hotmail.com', NULL),
(130, 'vivakn316@hotmail.com', NULL),
(131, 'wendolynkh887@hotmail.com', NULL),
(132, 'wondaow926@hotmail.com', NULL),
(133, 'veronawx474@hotmail.com', NULL),
(134, 'shanitanv224@hotmail.com', NULL),
(135, 'yesseniaoc159@hotmail.com', NULL),
(136, 'wanitajx045@hotmail.com', NULL),
(137, 'stephaniend676@hotmail.com', NULL),
(138, 'wendyxa553@hotmail.com', NULL),
(139, 'vikkimz542@hotmail.com', NULL),
(140, 'vernonag464@hotmail.com', NULL),
(141, 'yukitl734@hotmail.com', NULL),
(142, 'yurikopq134@hotmail.com', NULL),
(143, 'trinityfu907@hotmail.com', NULL),
(144, 'wynellpc522@hotmail.com', NULL),
(145, 'amm@gmail.com', NULL),
(146, 'sabbir@gmail.com', NULL),
(147, 'sabbir@gmail.com', NULL),
(148, 'samsu@gmail.com', NULL),
(149, 'karim@gmail.com', NULL),
(150, 'mukter@gmail.com', NULL),
(151, '', NULL),
(152, '', NULL),
(153, '', NULL),
(154, '', NULL),
(155, '', NULL),
(156, '', NULL),
(157, '', NULL),
(158, '', NULL),
(159, '', NULL),
(160, '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `genders`
--

CREATE TABLE `genders` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `deleted_at` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `genders`
--

INSERT INTO `genders` (`id`, `name`, `gender`, `deleted_at`) VALUES
(7, 'jamsed', 'female', NULL),
(8, 'kobita', 'female', NULL),
(9, 'sobita', 'female', NULL),
(10, 'bakkor', 'male', NULL),
(11, 'mukter', 'male', NULL),
(12, 'rashel', 'male', NULL),
(13, 'rohomot', 'male', NULL),
(14, 'rani', 'female', NULL),
(15, 'bfbf', 'others', NULL),
(16, 'cvbgf', 'male', NULL),
(17, 'vbbnvhfgh', 'female', NULL),
(18, 'erftertrtgy', 'female', NULL),
(19, 'cfbfbhf', 'female', NULL),
(20, 'bvbnhgh', 'male', NULL),
(21, 'bcvbfghf', 'others', NULL),
(22, 'xcvdfgfdg', 'male', NULL),
(23, 'xcvdfedf', 'female', NULL),
(24, 'ccsdf', 'male', NULL),
(25, 'vccvbcgb', 'male', NULL),
(26, 'gvbnghng', 'male', NULL),
(27, 'trtrty', 'others', NULL),
(28, 'cvbcvbvb', 'male', NULL),
(29, 'bvnvhng', 'female', NULL),
(30, 'xvcxv', 'male', NULL),
(31, 'cvgfgh', 'female', NULL),
(32, 'cvcvb', 'male', NULL),
(33, 'cvbvbn', 'male', NULL),
(34, 'cgftghf', 'female', NULL),
(35, 'cvbfgbhfg', 'others', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hobby`
--

CREATE TABLE `hobby` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `hobbies` varchar(255) NOT NULL,
  `deleted_at` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hobby`
--

INSERT INTO `hobby` (`id`, `name`, `hobbies`, `deleted_at`) VALUES
(16, 'arman', 'Coding,Reading,Swimming', NULL),
(17, 'sifat', 'Gardening,Playing Football,Coding', NULL),
(18, 'niloy', 'Gardening,Playing Football,Coding', NULL),
(19, 'roton', 'Gardening,Coding,Swimming', NULL),
(20, 'johan', 'Gardening,Playing Football,Coding,Reading', NULL),
(21, 'sonjoy', 'Reading,Swimming', NULL),
(22, 'alamin', 'Gardening,Playing Football', NULL),
(23, 'farid', 'Gardening,Playing Football,Coding', NULL),
(25, 'sumit', 'Gardening,Playing Football,Coding', NULL),
(26, 'selim', 'Gardening,Playing Football,Coding', NULL),
(28, 'bfbfhb', 'Gardening', NULL),
(30, 'saddam', 'Gardening,Playing Football,Coding,Reading', NULL),
(31, 'manik', 'Playing Football,Coding', NULL),
(32, 'motin', 'Gardening,Playing Football,Coding,Reading', NULL),
(33, 'mutalib', 'Gardening,Reading', NULL),
(34, 'sofik', 'Gardening,Coding,Reading', NULL),
(35, 'salek', 'Gardening,Playing Football,Coding', NULL),
(36, 'baten', 'Playing Football', NULL),
(37, 'hjmhj', 'Gardening,Playing Football', NULL),
(38, 'ssss', 'Playing Football,Coding', NULL),
(39, 'n', 'Gardening', NULL),
(40, 'mmmm', 'Playing Football', NULL),
(41, 'fghnf', 'Gardening', NULL),
(42, 'nadim', 'Gardening,Playing Football,Coding', NULL),
(43, 'nn', 'Playing Football,Coding', NULL),
(44, 'hgj', 'Gardening,Playing Football', NULL),
(45, 'ssssss', 'Gardening,Playing Football', NULL),
(46, 'sultan', 'Gardening,Playing Football', NULL),
(47, 'samsu', 'Gardening,Playing Football,Coding', NULL),
(48, 'Nasir', 'Gardening,Coding', NULL),
(49, 'Mamun', 'Reading,Swimming', NULL),
(50, 'Masum', 'Coding,Reading', NULL),
(51, 'mafuj', 'Playing Football,Reading', NULL),
(52, 'Shihab', 'Gardening,Playing Football,Coding,Reading,Swimming', NULL),
(53, 'dcdfvdfnjv', 'Gardening', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `profilepicture`
--

CREATE TABLE `profilepicture` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `images` varchar(255) NOT NULL,
  `deleted_at` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profilepicture`
--

INSERT INTO `profilepicture` (`id`, `name`, `images`, `deleted_at`) VALUES
(38, 'bb', '1467048207download.jpg', NULL),
(40, 'bbcc', '1467048227drupal-logo-small.jpg', NULL),
(41, 'ff', '1467048235images.jpg', NULL),
(42, 'nnn', '1467048243li-logo-small-drshdw.gif', NULL),
(43, 'ssss', '1467048253Neoseeker_logo-Light_reasonably_small.JPG', NULL),
(44, 'rr', '1467048263WWF-SMALL-LOGO.jpg', NULL),
(45, 'mmmm', '1467048471chrome_logo-100225494-small.jpg', NULL),
(46, 'aa', '1467048501chrome_logo-100225494-small.jpg', NULL),
(48, 'manik', '1467133752recyclelogo.gif', NULL),
(50, 'dfdfg', '1467133858compass-icon.png', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `summary`
--

CREATE TABLE `summary` (
  `id` int(11) NOT NULL,
  `summary` text NOT NULL,
  `deleted_at` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summary`
--

INSERT INTO `summary` (`id`, `summary`, `deleted_at`) VALUES
(2, 'dfgfgbhfhbfghb', NULL),
(3, 'fhgbfhfghfghnghjhjghmnhnmhjmhjmhkjhjkj', NULL),
(4, 'dfeuysbfdnfknvfhknxvmkcjvvcbvbvb', NULL),
(5, 'cbvcvbfvbvbnfvbdfsyfsyfcnbfcvcvccbcvbbn', NULL),
(6, 'vcvdfgvdisuyfnudcmvkznfnzvcnhjcxmc', NULL),
(7, 'xfcvsdgcbsfgncjfcgddchgmvncjxnghvbvc', NULL),
(8, 'gsdhcgsdhcjcjhdvdf', NULL),
(9, 'fdeufsgvdychjgvhfdsjkcvhxv', NULL),
(10, 'cvbvfcbvhkubfbsgnxdcasncdfvnsdnsuhzmhcnzcbsytbdfesdnksncvxcjbvxcv', NULL),
(11, 'vckudcbtdsnfdgybcfgdsjhcfbznxncguxncgdgcfbdgvbgxcvxcvcv', NULL),
(12, 'bcbvfduvgndvfgdsftysduhfgdusfxshdcfsdfvc', NULL),
(13, 'vbfdbvfgbfghfbgsfvgcgbnbvnvn', NULL),
(14, 'fgbfggbfbhbfyuhtfdssgvcvfgdcf', NULL),
(15, 'gdffvgfvgvccvgfcfbcb', NULL),
(16, 'gdfgvdfgfcvcfbcvvcvb', NULL),
(17, 'cvvcvccxvbcbvcvvcbcvbcvbvbfbhfg', NULL),
(18, 'vbcvbfbnkhkghfsdsffdcb', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `genders`
--
ALTER TABLE `genders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobby`
--
ALTER TABLE `hobby`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profilepicture`
--
ALTER TABLE `profilepicture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summary`
--
ALTER TABLE `summary`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `book`
--
ALTER TABLE `book`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=198;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=161;
--
-- AUTO_INCREMENT for table `genders`
--
ALTER TABLE `genders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `hobby`
--
ALTER TABLE `hobby`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `profilepicture`
--
ALTER TABLE `profilepicture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `summary`
--
ALTER TABLE `summary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
