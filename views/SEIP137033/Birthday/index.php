<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137033\Birthday\Birthday;
use App\Bitm\SEIP137033\Utility\Utility;
use App\Bitm\SEIP137033\Message\Message;

$birthday= new Birthday();
$birthday->prepare($_GET);
$allDate=$birthday->index();

?>

<!DOCTYPE html>
<html lang="en-US">
<head>
    <title>All Birthday List</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/css/w3.css">
    <link rel="stylesheet" href="../../../Resources/font-awesome-4.6.3/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../Resources/DataTables/jquery.dataTables.min.css">
    <script  src="../../../Resources/js/jquery-3.0.0.min.js"></script>
    <script type="text/javascript" src="../../../Resources/DataTables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function() {
            $('#myTable').DataTable();
        } );
    </script>

    <style>
        select{
            width: 4em;
            height: 2em;

        }
    </style>



</head>
<body>
    <nav class="w3-sidenav w3-white w3-card-2" style="display:none" id="mySidenav">
        <a href="javascript:void(0)"
           onclick="w3_close()"
           class="w3-closenav  w3-teal w3-xlarge"> Close &times;</a>
        <div class="w3-padding w3-center w3-teal">
            <img class="w3-circle" src="../../../Resources/Image1/emu1.jpg" alt="emran" style="width:100%">
        </div>
        <br>
        <a href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/index.php">Home</a>
        <a href="#">About Me</a>
        <a href="#">Contact</a>
        <a href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/views/SEIP137033/Book/index.php" target="_blank">Book</a>
        <a href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/views/SEIP137033/Hobby/index.php" target="_blank">Hobby</a>
        <a href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/views/SEIP137033/Newsletter/index.php" target="_blank">Email</a>
        <a href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/views/SEIP137033/ProfilePicture/index.php" target="_blank">ProfilePicture</a>
        <a href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/views/SEIP137033/Birthday/index.php" target="_blank">Birthday</a>
        <a href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/views/SEIP137033/Gender/index.php" target="_blank">Gender</a>
        <a href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/views/SEIP137033/City/index.php" target="_blank">City</a>
        <a href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/views/SEIP137033/Summary/index.php" target="_blank">Summary</a>

    </nav>

<div id="main">

    <header class="w3-container w3-teal">
        <h2>
            <span class="w3-opennav w3-xxlarge" onclick="w3_open()" id="openNav">&#9776;</span>
            All Birthday List
            <a class="w3-right" href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/"><i class="fa fa-home w3-xxlarge">&nbsp;Home</i></a>
        </h2>
    </header>
    <br>
    <br>

   <div class="w3-container">
       <a href="create.php"><button class="w3-btn w3-round-large w3-indigo">Create Again</button></a>
       <a href="trashed.php"> <button class="w3-btn w3-round-large w3-indigo">Trashed Item</button></a>
       <a href="pdf.php"><button class="w3-btn w3-round-large w3-indigo">Download as PDF</button></a>
       <a href="xls.php"><button class="w3-btn w3-round-large w3-indigo">download as xls</button></a>
       <a href="mail.php"><button class="w3-btn w3-round-large w3-indigo">Send as Email</button></a>
   </div>



<br/>

<div id="message">
    <?php
    if((array_key_exists('message',$_SESSION)) && (!empty($_SESSION['message']))){
        echo Message::message();
    }
    ?>
</div>
<br/>


<div class="w3-container">
    <table class="w3-table w3-striped w3-border w3-card-4" id="myTable">
        <thead>
        <tr class="w3-blue">
            <th>Serial</th>
            <th>ID</th>
            <th>Name</th>
            <th>Birthday</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $sl=0;
        foreach($allDate as $birthday){
            $sl++;
            $birthdate = $birthday->date;
            $time = strtotime($birthdate);
            $bdate = date("d/m/y", $time);

            ?>

            <tr>
                <td><?php echo $sl ?></td>
                <td><?php echo $birthday->id ?></td>
                <td><?php echo $birthday->name ?></td>
                <td><?php echo $bdate ?></td>
                <td>
                    <a href="view.php?id=<?php echo $birthday->id?>"><button class="w3-btn w3-round w3-teal">View</button></a>
                    <a href="edit.php?id=<?php echo $birthday->id?>"><button class="w3-btn w3-round w3-light-green">Edit</button></a>
                    <a href="delete.php?id=<?php echo $birthday->id?>"><button class="w3-btn w3-round w3-red" type="button" id="delete"  Onclick="return ConfirmDelete()">Delete</button></a>
                    <a href="trash.php?id=<?php echo $birthday->id?>"><button class="w3-btn w3-round  w3-amber">Move to Trash</button></a>
                </td>
            </tr>
        <?php } ?>

        </tbody>

    </table>



</div>

<footer class="w3-container w3-teal w3-bottom">
    <h3 class="w3-center"> BASIS BITM Atomic Project</h3>
</footer>
    <br>
    <br>
    <br>
    <br>
 </div>

<script>
    $('#message').show().delay(2000).fadeOut("slow");


    //        $(document).ready(function(){
    //            $("#delete").click(function(){
    //                if (!confirm("Do you want to delete")){
    //                    return false;
    //                }
    //            });
    //        });
    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }

    function w3_open() {
        document.getElementById("main").style.marginLeft = "25%";
        document.getElementById("mySidenav").style.width = "25%";
        document.getElementById("mySidenav").style.display = "block";
        document.getElementById("openNav").style.display = 'none';
    }
    function w3_close() {
        document.getElementById("main").style.marginLeft = "0%";
        document.getElementById("mySidenav").style.display = "none";
        document.getElementById("openNav").style.display = "inline-block";
    }
</script>

</body>
</html>

