<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137033\Birthday\Birthday;
//use App\Bitm\SEIP137033\Utility\Utility;
//use App\Bitm\SEIP137033\Message\Message;

$birthday= new Birthday();
$birthday->prepare($_GET);
//Utility::dd($birthday);
//die();

$singleItem=$birthday->view();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit Your Name and Birthday</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/css/w3.css"/>

</head>
<body>

    <div class="w3-container">
        <header class="w3-container w3-teal">
            <h2>Edit Name and Birthday</h2>
        </header>
        <form class="w3-container w3-card-4" action="update.php" method="POST">
            <h4>
                <label class="w3-text-blue"><b>Enter Your Name and Birthday:</b></label>
            </h4>
            <input class="w3-input w3-border" type="hidden" name="id"  value="<?php echo $singleItem->id?>"/><br/>
            <input class="w3-input w3-border" type="text" name="name"  placeholder="Enter Your Name" value="<?php echo $singleItem->name?>" required/><br/>

            <input class="w3-input w3-border" type="date" name="date" id="date" value="<?php echo $singleItem->date ?>" required/><br/>
            <button type="submit" class="w3-btn w3-blue">Update</button><br/><br/>
        </form>
    </div>

</body>
</html>
