<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137033\ProfilePicture\ImageUploader;

$profile_picture= new ImageUploader();
$single_info=$profile_picture->prepare($_GET)->view();


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit Name and Profile Picture </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/css/w3.css"/>
</head>
<body>

<div class="w3-container">
    <header class="w3-container w3-teal">
        <h2>Edit Name and Profile Picture</h2>
    </header>

    <form class="w3-container w3-card-4" action="update.php" method="POST" enctype="multipart/form-data">
        <h4>
            <label class="w3-text-blue"><b>Edit Your Name:</b></label>
        </h4>
        <input class="w3-input w3-border" type="hidden" name="id"  value="<?php echo $single_info->id?>"/><br/>
        <input class="w3-input w3-border"  type="text" name="name" value="<?php echo $single_info->name?>" >

        <h4>
            <label class="w3-text-blue"><b>Select Picture:</b></label>
        </h4>
        <input type="file" name="image" class="w3-input w3-border">
        <img src="../../../Resources/Images/<?php echo $single_info->images?>" height="100px" width="100px">
        
        <br>
        <br>

        <button type="submit" class="w3-btn w3-blue">Update</button>
        
        <br/>
        <br/>
        
    </form>
</div>

</body>
</html>

