<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137033\ProfilePicture\ImageUploader;

if((isset($_FILES['image'])&& !empty($_FILES['image']['name']))){
    $image_name= time().$_FILES['image']['name'];
    $temporary_location= $_FILES['image']['tmp_name'];

    move_uploaded_file( $temporary_location,'../../../Resources/Images/'.$image_name);
    $_POST['image']=$image_name;

}

$profile_picture= new ImageUploader();
$profile_picture->prepare($_POST)->store();

