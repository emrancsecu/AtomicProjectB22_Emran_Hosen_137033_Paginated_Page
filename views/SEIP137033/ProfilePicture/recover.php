<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137033\ProfilePicture\ImageUploader;

$profile_picture= new ImageUploader();
$profile_picture->prepare($_GET);
$profile_picture->recover();

?>