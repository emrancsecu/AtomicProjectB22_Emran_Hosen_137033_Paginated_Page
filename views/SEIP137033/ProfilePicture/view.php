<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137033\ProfilePicture\ImageUploader;

$profile_picture= new ImageUploader();
$single_info=$profile_picture->prepare($_GET)->view();


?>

<!DOCTYPE html>
<html lang="en-US">
<head>
    <title>View Name and Profile Picture</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/css/w3.css"/>
</head>
<body>

<div class="w3-container">
    <ul class="w3-ul w3-card-4">
        <h4><li class="w3-blue">View ID, Name and Profile Picture </li></h4>
        <li><?php echo $single_info->id;?></li>
        <li><?php echo $single_info->name;?></li>
        <br>
        <li><img src="../../../Resources/Images/<?php echo $single_info->images?>" height="80px" width="80px"></li>
        <br>
        <a href="index.php"><button class="w3-btn w3-round-large w3-indigo">Back to List</button></a>
        <br>

    </ul>

</div>


</body>
</html>


