<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137033\Hobby\Hobby;
use App\Bitm\SEIP137033\Message\Message;
use App\Bitm\SEIP137033\Utility\Utility;

$hobby= new Hobby();
$hobby->prepare($_GET);
$singleItem=$hobby->view();

$editItem=explode(",",$singleItem->hobbies);
//echo $singleItem->id;
//Utility::dd($singleItem);
//Utility::dd($editItem);
//die();

?>

<!DOCTYPE html>
<html lang="en-Us">
<head>
    <title>Edit Name and Hobby</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/css/w3.css"/>
</head>
<body>

<div class="w3-container">
    <header class="w3-container w3-teal">
        <h2>Edit name and Hobby</h2>
    </header>
    <form class="w3-container w3-card-4" action="update.php" method="POST">
        <h4>
            <label class="w3-text-blue"><b>Edit Your Name:</b></label>
        </h4>
        <input type="text" class="w3-input w3-border" name="name" placeholder="Enter your name" value="<?php echo $singleItem->name?>" required>
        <h5>
            <label class="w3-text-blue"><b>Edit Your Hobby:</b></label>
        </h5>

        <input class="w3-check" type="checkbox" name="hobby[]" value="Gardening" <?php if(in_array("Gardening",$editItem)){echo "checked";} ?> > Gardening
        <br/>
        <input class="w3-check" type="checkbox" name="hobby[]" value="Playing Football" <?php if(in_array("Playing Football",$editItem)){echo "checked";} ?>> Playing Football
        <br/>
        <input class="w3-check" type="checkbox" name="hobby[]" value="Coding" <?php if(in_array("Coding",$editItem)){echo "checked";} ?> > Coding
        <br/>
        <input class="w3-check" type="checkbox" name="hobby[]" value="Reading" <?php if(in_array("Reading",$editItem)){echo "checked";} ?> > Reading
        <br/>
        <input class="w3-check" type="checkbox" name="hobby[]" value="Swimming" <?php if(in_array("Swimming",$editItem)){echo "checked";} ?> > Swimming
        <br/>
        <input class="w3-check" type="hidden" name="id" id="hobbyid" value="<?php echo $singleItem->id?>">
        <br/>
        <button type="submit" class="w3-btn w3-blue">Submit</button><br/><br/>

        
    </form>
</div>

</body>
</html>
