<?php
include_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP137033\Hobby\Hobby;
$obj= new Hobby();
$allData=$obj->index();
//var_dump($allData);
$trs="";
$sl=0;
foreach($allData as $data):
    $sl++;
    $trs.="<tr>";
    $trs.="<td> $sl</td>";
    $trs.="<td> $data->id</td>";
    $trs.="<td> $data->name</td>";
    $trs.="<td> $data->hobbies</td>";
    $trs.="</tr>";
endforeach;
$html= <<<BITM
<div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>Serial</th>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Hobby List</th>
              </tr>
                </thead>
                <tbody>

                  $trs

                </tbody>
            </table>

BITM;


// Require composer autoload
require_once ('../../../vendor/mpdf/mpdf/mpdf.php');
//Create an instance of the class:

$mpdf = new mPDF();

// Write some HTML code:

$mpdf->WriteHTML($html);

// Output a PDF file directly to the browser
$mpdf->Output('list.pdf', 'D');