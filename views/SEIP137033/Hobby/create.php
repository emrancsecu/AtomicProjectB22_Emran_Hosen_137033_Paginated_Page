<!DOCTYPE html>
<html lang="en-US">
<head>
    <title>Create Name and Hobby</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/css/w3.css">
</head>

<body>
<div class="w3-container">
    <header class="w3-container w3-teal">
        <h2>Create Name and Hobby</h2>
    </header>

    <form class="w3-container w3-card-4" action="store.php" method="POST">
        <h4>
            <label class="w3-text-blue"><b>Enter Your Name:</b></label>
        </h4>
        <input class="w3-input w3-border" type="text" name="name"  placeholder="Enter Your Name" required /><br/>

        <h5>
            <label class="w3-text-blue"><b>Select Your Hobby:</b></label>
        </h5>

        <input class="w3-check" type="checkbox" name="hobby[]" value="Gardening"> Gardening
        <br/>
        <input class="w3-check" type="checkbox" name="hobby[]" value="Playing Football"> Playing Football
        <br/>
        <input class="w3-check" type="checkbox" name="hobby[]" value="Coding"> Coding
        <br/>
        <input class="w3-check" type="checkbox" name="hobby[]" value="Reading"> Reading
        <br/>
        <input class="w3-check" type="checkbox" name="hobby[]" value="Swimming"> Swimming
        <br/>
        <br/>
        <button type="submit" class="w3-btn w3-blue">Submit</button><br/><br/>
    </form>
</div>
</body>
</html>

