<!DOCTYPE html>
<html lang="en-US">
<head>
<title>Create Book Title</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../../../Resources/css/w3.css">
<link rel="stylesheet" href="../../../Resources/font-awesome-4.6.3/css/font-awesome.min.css">

</head>

<body>

    <div class="w3-container">
        <header class="w3-container w3-teal">
            <h2>
            <a class="w3-left" href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/"><i class="fa fa-home w3-xxxlarge"></i></a>
                &nbsp; Create Book Title

                <a class="w3-right w3-margin-right" href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/views/SEIP137033/Book/"><i class="fa fa-arrow-left w3-xxxlarge"></i></a>
            </h2>

        </header>

        <form class="w3-container w3-card-4" action="store.php" method="POST">
            <h4>
                <label class="w3-text-blue"><b>Enter Book Title</b></label>
            </h4>
            <input class="w3-input w3-border" type="text" name="title" id="title" placeholder="Enter Book Title" required /><br/>
            <button type="submit" class="w3-btn w3-blue">Submit</button>
            <br>
            <br>
        </form>

    </div>
</body>
</html>

