<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137033\Book\Book;
//use App\Bitm\SEIP137033\Utility\Utility;
//use App\Bitm\SEIP137033\Message\Message;

$book= new Book();
$book->prepare($_GET);
//Utility::dd($book);
//die();
$singleItem=$book->view();
?>

<!DOCTYPE html>
<html lang="en-Us">
<head>
    <title>Edit Book Title</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/css/w3.css"/>
</head>
<body>

<div class="w3-container">
    <header class="w3-container w3-teal">
        <h2>Edit Book Title</h2>
    </header>
    <form class="w3-container w3-card-4" action="update.php" method="POST">
        <h4>
            <label class="w3-text-blue"><b>Enter Book Title:</b></label>
        </h4>
        <input class="w3-input w3-border" type="hidden" name="id" id="title" value="<?php echo $singleItem->id?>"/><br/>
        <input class="w3-input w3-border" type="text" name="title" id="title" placeholder="Enter Book Title" value="<?php echo $singleItem->title?>" required/><br/>
        <button type="submit" class="w3-btn w3-blue">Submit</button><br/><br/>
    </form>
</div>

</body>
</html>
