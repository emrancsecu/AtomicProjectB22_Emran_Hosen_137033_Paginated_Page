<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137033\Book\Book;
use App\Bitm\SEIP137033\Utility\Utility;
use App\Bitm\SEIP137033\Message\Message;

$book= new Book();

if(array_key_exists('itemPerPage',$_SESSION)) {
    if (array_key_exists('itemPerPage', $_GET)) {
        $_SESSION['itemPerPage'] = $_GET['itemPerPage'];
    }
}
else{
    $_SESSION['itemPerPage']=5;
}

//Utility::dd($_SESSION['itemPerPage']);

$itemPerPage=$_SESSION['itemPerPage'];
$totalItem=$book->count();



$totalPage=ceil($totalItem/$itemPerPage);
//Utility::dd($itemPerPage);
$pagination="";


if(array_key_exists('pageNumber',$_GET)){
    $pageNumber=$_GET['pageNumber'];
}else{
    $pageNumber=1;
}

$previous="";
if($pageNumber>1){
    $prev=$pageNumber-1;
    $previous="<li><a href='index.php?pageNumber=$prev'>&#10094; Previous</a></li>";
}

$next="";
if($pageNumber<$totalPage){
    $next_item=$pageNumber+1;
    $next="<li><a href='index.php?pageNumber=$next_item'>Next &#10095;</a></li>";
}

for($i=1;$i<=$totalPage;$i++){
    $class=($pageNumber==$i)?"w3-blue":"w3-hover-red";
    $pagination.="<li><a  class='$class' href='index.php?pageNumber=$i'>$i</a></li>";

}




$pageStartFrom=$itemPerPage*($pageNumber-1);
$allBook=$book->paginator($pageStartFrom,$itemPerPage);

//Utility::dd($allBook);
//die();


?>


<!DOCTYPE html>
<html lang="en-US">
<head>
    <title>All Book List</title>
    <meta charset="utf-8">   
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/css/w3.css">
    <link rel="stylesheet" href="../../../Resources/font-awesome-4.6.3/css/font-awesome.min.css">
    <script  src="../../../Resources/js/jquery-3.0.0.min.js"></script>
    <style>
        select { width:5.5em; height: 2.2em }
    </style>

</head>
<body>
    <nav class="w3-sidenav w3-white w3-card-2" style="display:none" id="mySidenav">
        <a href="javascript:void(0)"
           onclick="w3_close()"
           class="w3-closenav  w3-teal w3-xlarge"> Close &times;</a>
        <div class="w3-padding w3-center w3-teal">
            <img class="w3-circle" src="../../../Resources/Image1/emu1.jpg" alt="emran" style="width:100%">
        </div>
        <br>
        <a href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/index.php">Home</a>
        <a href="#">About Me</a>
        <a href="#">Contact</a>
        <a href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/views/SEIP137033/Book/index.php" target="_blank">Book</a>
        <a href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/views/SEIP137033/Hobby/index.php" target="_blank">Hobby</a>
        <a href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/views/SEIP137033/Newsletter/index.php" target="_blank">Email</a>
        <a href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/views/SEIP137033/ProfilePicture/index.php" target="_blank">ProfilePicture</a>
        <a href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/views/SEIP137033/Birthday/index.php" target="_blank">Birthday</a>
        <a href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/views/SEIP137033/Gender/index.php" target="_blank">Gender</a>
        <a href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/views/SEIP137033/City/index.php" target="_blank">City</a>
        <a href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/views/SEIP137033/Summary/index.php" target="_blank">Summary</a>

    </nav>

    <div id="main">

        <header class="w3-container w3-teal">
            <h2>
                <span class="w3-opennav w3-xxlarge" onclick="w3_open()" id="openNav">&#9776;</span>
                All Book List
                <a class="w3-right" href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/"><i class="fa fa-home w3-xxlarge">&nbsp;Home</i></a>
            </h2>
        </header>
        <br>
        <div class="w3-container">

         <a href="create.php"><button class="w3-btn w3-round-large w3-indigo">Create Again</button></a>
         <a href="trashed.php"> <button class="w3-btn w3-round-large w3-indigo">Trashed Item</button></a>


    <form class="w3-container" style="float:right" >
        <label class="w3-text-black"><b>Select how many items you want to show (select one):</b></label>
        <select class="w3-container" name="itemPerPage">
            <option <?php if($itemPerPage == 5) echo "selected" ?> >5</option>
            <option <?php if($itemPerPage == 10) echo "selected" ?> >10</option>
            <option <?php if($itemPerPage == 15) echo "selected" ?> >15</option>
            <option <?php if($itemPerPage == 20) echo "selected" ?> >20</option>
            <option <?php if($itemPerPage == 25) echo "selected" ?> >25</option>
            <option <?php if($itemPerPage == 30) echo "selected" ?> >30</option>
            <option <?php if($itemPerPage == 35) echo "selected" ?> >35</option>
            <option <?php if($itemPerPage == 40) echo "selected" ?> >40</option>
            <option <?php if($itemPerPage == 45) echo "selected" ?> >45</option>
            <option <?php if($itemPerPage == 50) echo "selected" ?> >50</option>
        </select>
        <button type="submit" class="w3-btn w3-blue">Submit</button>

        </form>
     </div>
    
    
    <br/>

    <div id="message">
        <?php
        if((array_key_exists('message',$_SESSION)) && (!empty($_SESSION['message']))){
            echo Message::message();
        }
        ?>
    </div>
    <br/>

    <div class="w3-container">
        <table class="w3-table w3-striped w3-border w3-card-4">
            <thead>
            <tr class="w3-blue">
                <th>Serial</th>
                <th>ID</th>
                <th>Book Title</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $sl=0;
            foreach($allBook as $book){
            $sl++;
            ?>
            <tr>
                <td><?php echo $sl+$pageStartFrom ?></td>
                <td><?php echo $book->id ?></td>
                <td><?php echo $book->title ?></td>
                <td>
                    <a href="view.php?id=<?php echo $book->id?>"><button class="w3-btn w3-round w3-teal">View</button></a>
                    <a href="edit.php?id=<?php echo $book->id?>"><button class="w3-btn w3-round w3-light-green">Edit</button></a>
                    <a href="delete.php?id=<?php echo $book->id?>"><button class="w3-btn w3-round w3-red" type="button" id="delete"  Onclick="return ConfirmDelete()">Delete</button></a>
                    <a href="trash.php?id=<?php echo $book->id?>"><button class="w3-btn w3-round  w3-amber">Move to Trash</button></a>
                </td>
                </tr>
            <?php } ?>

            </tbody>

        </table>

         <div class="w3-container w3-margin-top w3-center">
        <a href="pdf.php"><button class="w3-btn w3-round-large w3-indigo">Download as PDF</button></a>
        <a href="xls.php"><button class="w3-btn w3-round-large w3-indigo">download as xls</button></a>
        <a href="create.php"><button class="w3-btn w3-round-large w3-indigo">Send as Email</button></a>
         </div>

        <div class="w3-center" style="margin-top: 20px">

            <ul class="w3-pagination w3-border w3-round">
                <?php echo $previous?>
                <?php echo $pagination?>
                <?php echo $next?>

            </ul>

        </div>

    </div>



    <footer class="w3-container w3-teal w3-bottom">
        <h3 class="w3-center"> BASIS BITM Atomic Project</h3>
    </footer>
        <br>
        <br>
        <br>
        <br>
 </div>


    <script>
        $('#message').show().delay(2000).fadeOut("slow");


        //        $(document).ready(function(){
        //            $("#delete").click(function(){
        //                if (!confirm("Do you want to delete")){
        //                    return false;
        //                }
        //            });
        //        });
        function ConfirmDelete()
        {
            var x = confirm("Are you sure you want to delete?");
            if (x)
                return true;
            else
                return false;
        }

        function w3_open() {
            document.getElementById("main").style.marginLeft = "25%";
            document.getElementById("mySidenav").style.width = "25%";
            document.getElementById("mySidenav").style.display = "block";
            document.getElementById("openNav").style.display = 'none';
        }
        function w3_close() {
            document.getElementById("main").style.marginLeft = "0%";
            document.getElementById("mySidenav").style.display = "none";
            document.getElementById("openNav").style.display = "inline-block";
        }
    </script>
</body>
</html>

