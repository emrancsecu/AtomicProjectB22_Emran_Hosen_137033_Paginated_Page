<!DOCTYPE html>
<html lang="en-US">
<head>
    <title>Add City and Name </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/css/w3.css">
<body>

<div class="w3-container">

    <header class="w3-container w3-teal">
        <h2>Add Your Name and City</h2>
    </header>


    <form class="w3-container w3-card-4" action="store.php" method="POST">
        <h4>
            <label class="w3-text-blue"><b>Enter Your Name:</b></label>
        </h4>
        <input class="w3-input w3-border" type="text" name="name" id="name" placeholder="Enter Your Name" required />

        <h4>
            <label class="w3-text-blue"><b>Select Your City from the below list (select one):</b></label>
        </h4>

        <select class="w3-select w3-border" name="city">
            <option disabled selected value>---Select Your City--</option>
            <option>Dhaka</option>
            <option>Chittagong</option>
            <option>Rajshahi</option>
            <option>Comilla</option>
            <option>Barisal</option>
            <option>Khulna</option>
            <option>Mymensingh</option>
            <option>Shylhet</option>
            <option>Kishorgonj</option>
        </select>

        <br/>
        <br/>

        <button type="submit" class="w3-btn w3-blue">Submit</button><br/><br/>
    </form>

</div>

</body>
</html>