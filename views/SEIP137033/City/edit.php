<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137033\City\City;
//use App\Bitm\SEIP137033\Utility\Utility;
//use App\Bitm\SEIP137033\Message\Message;

$city= new City();
$city->prepare($_GET);
//Utility::dd($town);
//die();
$singleItem=$city->view();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit Name and City</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/css/w3.css"/>
</head>
<body>


    <div class="w3-container">
        <header class="w3-container w3-teal">
            <h2>Edit Name and City</h2>
        </header>
        <form class="w3-container w3-card-4" action="update.php" method="POST">
            <h4>
                <label class="w3-text-blue"><b>Edit Your Name:</b></label>
            </h4>
            <input class="w3-input w3-border" type="hidden" name="id"  value="<?php echo $singleItem->id?>">

            <input type="text" class="form-control" name="name" placeholder="Enter Your Name" value="<?php echo $singleItem->name?>" required>
            <br/>
            <h4>
                <label class="w3-text-blue"><b>Select Your City from the below list (select one):</b></label>
            </h4>

            <select class="w3-select w3-border"  name="city">
                <option  value="<?php echo $singleItem->city?>"><?php echo $singleItem->city?></option>
                <option>Dhaka</option>
                <option>Chittagong</option>
                <option>Rajshahi</option>
                <option>Comilla</option>
                <option>Barisal</option>
                <option>Khulna</option>
                <option>Mymensingh</option>
                <option>Shylhet</option>
                <option>Kishorgonj</option>
            </select>
            <br/>
            <br/>

            <button type="submit" class="w3-btn w3-blue">Update</button><br/><br/>
        </form>
    </div>

</div>

</body>
</html>