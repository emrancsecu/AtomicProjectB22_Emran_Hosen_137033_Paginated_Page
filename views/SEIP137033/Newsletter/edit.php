<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137033\Newsletter\Newsletter;
use App\Bitm\SEIP137033\Utility\Utility;
use App\Bitm\SEIP137033\Message\Message;

$email= new Newsletter();
$email->prepare($_GET);
//Utility::dd($book);
//die();
$singleItem=$email->view();
?>

<!DOCTYPE html>
<html lang="en-Us">
<head>
    <title>Edit Email</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/css/w3.css"/>
</head>
<body>

<div class="w3-container">
    <header class="w3-container w3-teal">
        <h2>Edit Email</h2>
    </header>
    <form class="w3-container w3-card-4" action="update.php" method="POST">
        <h4>
            <label class="w3-text-blue"><b>Enter Your Email:</b></label>
        </h4>
        <input class="w3-input w3-border" type="hidden" name="id" id="title" value="<?php echo $singleItem->id?>"/><br/>
        <input class="w3-input w3-border" type="Email" name="email" id="email" placeholder="Enter Your Email" value="<?php echo $singleItem->email?>" required/><br/>
        <button type="submit" class="w3-btn w3-blue">Update</button>
        <br/>
        <br/>
    </form>

</div>

</body>
</html>
