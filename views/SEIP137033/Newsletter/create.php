<!DOCTYPE html>
<html lang="en-US">
<head>
    <title>Add Email Id</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/css/w3.css">
</head>

<body>
<div class="w3-container">
    <header class="w3-container w3-teal">
        <h2>Add Email Id</h2>
    </header>

    <form class="w3-container w3-card-4" action="store.php" method="POST">
        <h4>
            <label class="w3-text-blue"><b>Enter Your Email:</b></label>
        </h4>
        <input class="w3-input w3-border" type="Email" name="email" id="email" placeholder="Enter Your Email" required /><br/>
        <button type="submit" class="w3-btn w3-blue">Submit</button><br/><br/>
    </form>
</div>
</body>
</html>

