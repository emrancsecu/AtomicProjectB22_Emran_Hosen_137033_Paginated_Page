<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137033\Newsletter\Newsletter;
use App\Bitm\SEIP137033\Utility\Utility;
use App\Bitm\SEIP137033\Message\Message;

$email= new Newsletter();
$allEmail=$email->trashed();
//Utility::dd($allEmail);

?>

<!DOCTYPE html>
<html>
<head>
    <title>All Trashed Item List</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="../../../Resources/css/w3.css">
    <link rel="stylesheet" href="../../../Resources/font-awesome-4.6.3/css/font-awesome.min.css">
    <script  src="../../../Resources/js/jquery-3.0.0.min.js"></script>
</head>
<body>
    <nav class="w3-sidenav w3-white w3-card-2" style="display:none" id="mySidenav">
        <a href="javascript:void(0)"
           onclick="w3_close()"
           class="w3-closenav  w3-teal w3-xlarge"> Close &times;</a>
        <div class="w3-padding w3-center w3-teal">
            <img class="w3-circle" src="../../../Resources/Image1/emu1.jpg" alt="emran" style="width:100%">
        </div>
        <br>
        <a href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/index.php">Home</a>
        <a href="#">About Me</a>
        <a href="#">Contact</a>
        <a href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/views/SEIP137033/Book/index.php" target="_blank">Book</a>
        <a href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/views/SEIP137033/Hobby/index.php" target="_blank">Hobby</a>
        <a href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/views/SEIP137033/Newsletter/index.php" target="_blank">Email</a>
        <a href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/views/SEIP137033/ProfilePicture/index.php" target="_blank">ProfilePicture</a>
        <a href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/views/SEIP137033/Birthday/index.php" target="_blank">Birthday</a>
        <a href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/views/SEIP137033/Gender/index.php" target="_blank">Gender</a>
        <a href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/views/SEIP137033/City/index.php" target="_blank">City</a>
        <a href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/views/SEIP137033/Summary/index.php" target="_blank">Summary</a>

    </nav>

    <div id="main">

        <header class="w3-container w3-teal">
            <h2>
                <span class="w3-opennav w3-xxlarge" onclick="w3_open()" id="openNav">&#9776;</span>
                All Trashed List
                <a class="w3-right" href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/"><i class="fa fa-home w3-xxlarge">&nbsp;Home</i></a>
            </h2>
        </header>
        <br>
        <br>

    <div class="w3-container">

    <form class="w3-container" action="recoverselected.php" method="post" id="multiple">
        <a href="create.php"><button class="w3-btn w3-round-xlarge w3-indigo" type="button">Create</button></a>
        <a href="index.php"><button class="w3-btn w3-round-xlarge w3-indigo" type="button" >Back To List</button></a>
        <button class="w3-btn w3-round-xlarge w3-indigo" type="submit">Recover Selected</button>
        <button class="w3-btn w3-round-xlarge w3-indigo" type="button" id="multiple_delete">Deleted Selected</button>
        <br/><br/>

        <table class="w3-table w3-striped w3-border w3-card-4">
            <thead>
            <tr class="w3-blue">
                <th>Check Item</th>
                <th>Serial</th>
                <th>ID</th>
                <th>Email Id</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $sl=0;
            foreach($allEmail as $email){
                $sl++;
                ?>
                <tr>
                    <td><input class="w3-check" type="checkbox" name="mark[]" value="<?php echo $email['id'] ?>"></td>
                    <td><?php echo $sl ?></td>
                    <td><?php echo $email['id'] ?></td>
                    <td><?php echo $email['email'] ?></td>
                    <td>
                        <a href="recover.php?id=<?php echo $email['id']?>"><button class="w3-btn w3-round w3-teal" type="button">Recover</button></a>
                        <a href="delete.php?id=<?php echo $email['id']?>"><button class="w3-btn w3-round w3-red" type="button" id="delete"  Onclick="return ConfirmDelete()">Delete</button></a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
</div>
        <footer class="w3-container w3-teal w3-bottom">
            <h3 class="w3-center"> BASIS BITM Atomic Project</h3>
        </footer>
</div>

<script>
    $('#message').show().delay(2000).fadeOut();


    $(document).ready(function(){
        $("#delete").click(function(){
            if (!confirm("Do you want to delete")){
                return false;
            }
        });
    });

    $('#multiple_delete').on('click',function(){
        document.forms[0].action="deleteselected.php";
        $('#multiple').submit();
    });

    function w3_open() {
        document.getElementById("main").style.marginLeft = "25%";
        document.getElementById("mySidenav").style.width = "25%";
        document.getElementById("mySidenav").style.display = "block";
        document.getElementById("openNav").style.display = 'none';
    }
    function w3_close() {
        document.getElementById("main").style.marginLeft = "0%";
        document.getElementById("mySidenav").style.display = "none";
        document.getElementById("openNav").style.display = "inline-block";
    }

</script>

</body>
</html>

