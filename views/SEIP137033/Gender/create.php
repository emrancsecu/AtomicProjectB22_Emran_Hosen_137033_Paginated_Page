<!DOCTYPE html>
<html lang="en-US">
<head>
    <title>Add Name and Gender </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/css/w3.css">
<body>

<div class="w3-container">

    <header class="w3-container w3-teal">
        <h2>Add Your Name and Gender</h2>
    </header>


    <form class="w3-container w3-card-4" action="store.php" method="post">
        <h4>
            <label class="w3-text-blue"><b>Enter Your Name:</b></label>
        </h4>
        <input class="w3-input w3-border" type="text" name="name"  placeholder="Enter Your Name" required />

        <h4>
            <label class="w3-text-blue"><b>Select Your Gender from the below list (select one):</b></label>
        </h4>

        <p>
            <input class="w3-radio" type="radio" name="gender" value="male">
            <label class="w3-validate">Male</label></p>

        <p>
            <input class="w3-radio" type="radio" name="gender" value="female">
            <label class="w3-validate">Female</label></p>
        <p>
            <input class="w3-radio" type="radio" name="gender" value="others">
            <label class="w3-validate">Others</label></p>

        <br/>

        <button type="submit" class="w3-btn w3-blue">Submit</button><br/><br/>
    </form>

</div>

</body>
</html>