<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137033\Gender\Gender;
//use App\Bitm\SEIP137033\Utility\Utility;
//use App\Bitm\SEIP137033\Message\Message;

$gender= new Gender();
$gender->prepare($_GET);
//Utility::dd($town);
//die();
$singleItem=$gender->view();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit Name and Gender</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/css/w3.css"/>
</head>
<body>


<div class="w3-container">
    <header class="w3-container w3-teal">
        <h2>Edit Name and Gender</h2>
    </header>
    <form class="w3-container w3-card-4" action="update.php" method="POST">
        <h4>
            <label class="w3-text-blue"><b>Edit Your Name:</b></label>
        </h4>
        <input class="w3-input w3-border" type="hidden" name="id"  value="<?php echo $singleItem->id?>">

        <input type="text" class="w3-input w3-border" name="name" placeholder="Enter Your Name" value="<?php echo $singleItem->name?>" required>

        <h4>
            <label class="w3-text-blue"><b>Your Selected Gender was:</b></label>
        </h4>

        <input type="text" class="w3-input w3-border"  value="<?php echo $singleItem->gender?>" >

        <h4>
            <label class="w3-text-blue"><b>Select Your Gender from the below list (select one):</b></label>
        </h4>

        <p>
            <input class="w3-radio" type="radio" name="gender" value="male" <?php if($singleItem->gender=="male") echo "checked"?> >
            <label class="w3-validate">Male</label></p>

        <p>
            <input class="w3-radio" type="radio" name="gender" value="female" <?php if($singleItem->gender=="female") echo "checked"?>>
            <label class="w3-validate">Female</label></p>
        <p>
            <input class="w3-radio" type="radio" name="gender" value="others" <?php if($singleItem->gender=="others") echo "checked"?>>
            <label class="w3-validate">Others</label></p>

        <br/>

        <button type="submit" class="w3-btn w3-blue">Update</button><br/><br/>
    </form>
</div>

</div>

</body>
</html>