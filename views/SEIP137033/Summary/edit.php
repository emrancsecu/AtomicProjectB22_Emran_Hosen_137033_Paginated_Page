<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137033\Summary\Summary;
use App\Bitm\SEIP137033\Utility\Utility;
use App\Bitm\SEIP137033\Message\Message;

$summary= new Summary();
$summary->prepare($_GET);
//Utility::dd($summary);
//die();
$singleItem=$summary->view();
?>

<!DOCTYPE html>
<html lang="en-Us">
<head>
    <title>Edit Summary</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/css/w3.css"/>
</head>
<body>

<div class="w3-container">
    <header class="w3-container w3-teal">
        <h2>Edit Summary</h2>
    </header>
    <form class="w3-container w3-card-4" action="update.php" method="POST">
        <h4>
            <label class="w3-text-blue"><b>Update or Change Summary :</b></label>
        </h4>

        <input class="w3-input w3-border" type="hidden" name="id"  value="<?php echo $singleItem->id?>"/><br/>

        <input class="w3-input w3-border"  name="summary"  placeholder="Edit Summary" value="<?php echo $singleItem->summary?>" required/><br/>
        <button type="submit" class="w3-btn w3-blue">Update</button>
        <br/>
        <br/>
    </form>

</div>

</body>
</html>
