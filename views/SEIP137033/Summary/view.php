<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137033\Summary\Summary;
use App\Bitm\SEIP137033\Utility\Utility;
use App\Bitm\SEIP137033\Message\Message;

$summary= new Summary();
$summary->prepare($_GET);
//Utility::dd($summary);
//die();
$singleItem=$summary->view();
?>

<!DOCTYPE html>
<html lang="en-US">
<head>
    <title>View summary and ID</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/css/w3.css"/>
</head>
<body>

<div class="w3-container">
    <ul class="w3-ul w3-card-4">
        <h4><li class="w3-blue">View ID and Summary </li></h4>
        <li><?php echo $singleItem->id;?></li>
        <li><?php echo $singleItem->summary;?></li>
        <br>
        <a href="index.php"><button class="w3-btn w3-round-large w3-indigo">Back to List</button></a>
        <br>

    </ul>

</div>


</body>
</html>


