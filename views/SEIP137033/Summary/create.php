<!DOCTYPE html>
<html lang="en-US">
<head>
    <title>Create organization Summary</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/css/w3.css">
</head>

<body>

<div class="w3-container">
    <header class="w3-container w3-teal">
        <h2>Create Organization Summary</h2>
    </header>

    <form class="w3-container w3-card-4" action="store.php" method="POST">
        <h4>
            <label class="w3-text-blue"><b>Add Summary:</b></label>
        </h4>
        <textarea class="w3-input w3-border" rows="10" placeholder="Write Your Summary here....." name="summary" required></textarea>

        <br/>
        <button type="submit" class="w3-btn w3-blue">Submit</button><br/><br/>
    </form>
</div>
</body>
</html>

