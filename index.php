<!DOCTYPE html>
<html>
<title>Atomic Project Home Page</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="Resources/css/w3.css">

<body >

    <nav class="w3-sidenav w3-cyan w3-card-2" style="display:none" id="mySidenav">
        <a href="javascript:void(0)"
           onclick="w3_close()"
           class="w3-closenav  w3-large w3-right"> Close &times;</a>
        <div class="w3-padding w3-center w3-teal">
            <img class="w3-circle" src="Resources/Image1/emu1.jpg" alt="emran" style="width:100%">
        </div>
        <br>
        <a href="index.php">Home</a>
        <a href="views/SEIP137033/Book/index.php" target="_blank">Book</a>
        <a href="views/SEIP137033/Hobby/index.php" target="_blank">Hobby</a>
        <a href="views/SEIP137033/Newsletter/index.php" target="_blank">Email</a>
        <a href="views/SEIP137033/ProfilePicture/index.php" target="_blank">ProfilePicture</a>
        <a href="views/SEIP137033/Birthday/index.php" target="_blank">Birthday</a>
        <a href="views/SEIP137033/Gender/index.php" target="_blank">Gender</a>
        <a href="views/SEIP137033/City/index.php" target="_blank">City</a>
        <a href="views/SEIP137033/Summary/index.php" target="_blank">Summary</a>

    </nav>

    <nav class="w3-sidenav w3-cyan w3-card-2 w3-animate-right" style="display:none;right:0;" id="rightMenu">
        <a href="javascript:void(0)" onclick="closeRightMenu()"
           class="w3-closenav w3-xlarge w3-blue-grey"> &times;</a>
        <a href="about.php">About Project</a>
        <a href="contact.php">Contact</a>
    </nav>




    <div id="main">

    <header class="w3-container w3-teal">
        <h1>
            <span class="w3-opennav w3-xxlarge" onclick="w3_open()" id="openNav">&#9776;</span>
            Atomic Project Home Page

            <span class="w3-btn-floating-large w3-amber w3-right" onclick="openRightMenu()"><i>+</i></span>
        </h1>
    </header>


<div class="w3-container " style="margin-top: 30px;">

    <table class="w3-table w3-striped w3-border w3-card-4 ">
        <thead>
        <tr class="w3-blue">
            <th>Serial</th>
            <th>Project Module Name</th>
            <th>View</th>
        </tr>
        </thead>
        <tr>
            <td>1</td>
            <td>Book</td>
            <td><a href="views/SEIP137033/Book/" target="_blank"><button class="w3-btn w3-round w3-indigo">View</button></a></td>
        </tr>
        <tr>
            <td>2</td>
            <td>Hobby</td>
            <td><a href="views/SEIP137033/Hobby/" target="_blank"><button class="w3-btn w3-round w3-indigo">View</button></a></td>
        </tr>

        <tr>
            <td>3</td>
            <td>Email</td>
            <td><a href="views/SEIP137033/Newsletter/" target="_blank"><button class="w3-btn w3-round w3-indigo">View</button></a></td>
        </tr>



        <tr>
            <td>4</td>
            <td>Profile Picture</td>
            <td><a href="views/SEIP137033/ProfilePicture/" target="_blank"><button class="w3-btn w3-round w3-indigo">View</button></a></td>
        </tr>

        <tr>
            <td>5</td>
            <td>Birthday</td>
            <td><a href="views/SEIP137033/Birthday/" target="_blank"><button class="w3-btn w3-round w3-indigo">View</button></a></td>
        </tr>

        <tr>
            <td>6</td>
            <td>Gender</td>
            <td><a href="views/SEIP137033/Gender/" target="_blank"><button class="w3-btn w3-round w3-indigo">View</button></a></td>
        </tr>

        <tr>
            <td>7</td>
            <td>City</td>
            <td><a href="views/SEIP137033/City/" target="_blank"><button class="w3-btn w3-round w3-indigo">View</button></a></td>
        </tr>

        <tr>
            <td>8</td>
            <td>Summary</td>
            <td><a href="views/SEIP137033/Summary/" target="_blank"><button class="w3-btn w3-round w3-indigo">View</button></a></td>
        </tr>



    </table>
    <br>
    <br>
    <br>
    <br>

</div>

<footer class="w3-container w3-teal w3-theme w3-bottom">
    <h3 class="w3-center"> BASIS BITM Atomic Project</h3>
</footer>

</div>

<script>
    function w3_open() {
        document.getElementById("main").style.marginLeft = "15%";
        document.getElementById("mySidenav").style.width = "15%";
        document.getElementById("mySidenav").style.display = "block";
        document.getElementById("openNav").style.display = 'none';

    }
    function w3_close() {
        document.getElementById("main").style.marginLeft = "0%";
        document.getElementById("mySidenav").style.display = "none";
        document.getElementById("openNav").style.display = "inline-block";

    }

    function openRightMenu() {
        document.getElementById("rightMenu").style.display = "block";
    }
    function closeRightMenu() {
        document.getElementById("rightMenu").style.display = "none";
    }



</script>

</body>

