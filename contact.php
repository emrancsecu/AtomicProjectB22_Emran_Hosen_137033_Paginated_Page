<!DOCTYPE html>
<html>
<title>Atomic Project Home Page</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="Resources/css/w3.css">

<body >

<nav class="w3-sidenav w3-cyan w3-card-2" style="display:none" id="mySidenav">
    <a href="javascript:void(0)"
       onclick="w3_close()"
       class="w3-closenav  w3-large w3-right"> Close &times;</a>
    <div class="w3-padding w3-center w3-teal">
        <img class="w3-circle" src="Resources/Image1/emu1.jpg" alt="emran" style="width:100%">
    </div>
    <br>
    <a href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/index.php">Home</a>
    <a href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/views/SEIP137033/Book/index.php" target="_blank">Book</a>
    <a href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/views/SEIP137033/Hobby/index.php" target="_blank">Hobby</a>
    <a href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/views/SEIP137033/Newsletter/index.php" target="_blank">Email</a>
    <a href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/views/SEIP137033/ProfilePicture/index.php" target="_blank">ProfilePicture</a>
    <a href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/views/SEIP137033/Birthday/index.php" target="_blank">Birthday</a>
    <a href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/views/SEIP137033/Gender/index.php" target="_blank">Gender</a>
    <a href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/views/SEIP137033/City/index.php" target="_blank">City</a>
    <a href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/views/SEIP137033/Summary/index.php" target="_blank">Summary</a>

</nav>

<nav class="w3-sidenav w3-cyan w3-card-2 w3-animate-right" style="display:none;right:0;" id="rightMenu">
    <a href="javascript:void(0)" onclick="closeRightMenu()"
       class="w3-closenav w3-xlarge w3-blue-grey"> &times;</a>
    <a href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/about.php">About Project</a>
    <a href="#">Contact</a>
    <a href="http://localhost/B22_Practice/AtomicProjectB22_Emran_Hosen_137033/index.php">Back</a>
</nav>


<div id="main">

    <header class="w3-container w3-teal">
        <h1>
            <span class="w3-opennav w3-xxlarge" onclick="w3_open()" id="openNav">&#9776;</span>
            Atomic Project Home Page

            <span class="w3-btn-floating-large w3-amber w3-right" onclick="openRightMenu()"><i>+</i></span>
        </h1>
    </header>


    <div action="#" class="w3-card-4">

        <div class="w3-container w3-brown">
            <h2>Contact Form</h2>
        </div>
        <form class="w3-container" action="#">

                <h5 class="w3-text-teal">GET IN TOUCH </h5>
                <p><label class="w3-label w3-text-brown"><b>First Name</b></label>
                <input class="w3-input w3-border w3-sand" name="first" type="text" required="required"></p>

                <p><label class="w3-label w3-text-brown"><b>Last Name</b></label>
                <input class="w3-input w3-border w3-sand" name="last" type="text" required="required"></p>

               <p><label class="w3-label w3-text-brown"><b>Email</b></label>
                <input class="w3-input w3-border w3-sand" name="email" type="email" required="required"></p>

                <p><label class="w3-label w3-text-brown"><b>Your Message</b></label>
                <textarea class="w3-input w3-border w3-sand" name="comment" type="text" required="required" rows="3"></textarea></p>

                 <p><button class="w3-btn w3-brown w3-right">Send</button></p>
                  <br/> <br/>  <br/> <br/> <br/>  <br/>


        </form>

    </div>







    <footer class="w3-container w3-teal w3-theme w3-bottom">
        <h3 class="w3-center"> BASIS BITM Atomic Project</h3>
    </footer>

</div>

<script>
    function w3_open() {
        document.getElementById("main").style.marginLeft = "15%";
        document.getElementById("mySidenav").style.width = "15%";
        document.getElementById("mySidenav").style.display = "block";
        document.getElementById("openNav").style.display = 'none';

    }
    function w3_close() {
        document.getElementById("main").style.marginLeft = "0%";
        document.getElementById("mySidenav").style.display = "none";
        document.getElementById("openNav").style.display = "inline-block";

    }

    function openRightMenu() {
        document.getElementById("rightMenu").style.display = "block";
    }
    function closeRightMenu() {
        document.getElementById("rightMenu").style.display = "none";
    }



</script>

</body>

